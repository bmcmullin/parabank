git diff --name-only HEAD~1 HEAD > changed_files.txt
for /f "tokens=*" %%a in (changed_files.txt) do (echo path:C:\GitLab-Runner\builds\KzGcjtMs\0\bmcmullin\parabank/%%a) >> changed_files.lst
@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION 
SET "filename1=changed_files.lst"
SET "outfile=scope.lst"
(
 FOR /f "usebackqdelims=" %%a IN ("%filename1%") DO (
  SET "line=%%a"
  SET "line=!line:\=/!"
  ECHO !line!
 )
)>"%outfile%"
